// Data List Comment
var listComment = [{
    Id: 1,
    CreatedDate: '20/8/2020 13:30PM',
    CreatedBy: 'User1',
    CommentContent: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum maxime soluta voluptates quidem ratione ip',
    ReplyCommentId: null,
    RequestId: 2,
    Avatar: "http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg"
  },
  {
    Id: 2,
    CreatedDate: '21/8/2020 12:30PM',
    CreatedBy: 'User2',
    CommentContent: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum maxime soluta voluptates quidem ratione ip',
    ReplyCommentId: 1,
    RequestId: 2,
    Avatar: "http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg"
  },
  {
    Id: 3,
    CreatedDate: '20/8/2020 13:30PM',
    CreatedBy: 'User3',
    CommentContent: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum maxime soluta voluptates quidem ratione ip',
    ReplyCommentId: null,
    RequestId: 2,
    Avatar: "http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg"
  },
  {
    Id: 5,
    CreatedDate: '20/8/2020 13:30PM',
    CreatedBy: 'User5',
    CommentContent: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum maxime soluta voluptates quidem ratione ip',
    ReplyCommentId: 3,
    RequestId: 2,
    Avatar: "http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg"
  },
];
//Declare currentUser
var currentUser = {
    Name : 'Current User',
    Avatar: "http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg"
}
//function Show or Hiden All Form Send
function showOrHidenAllFormSend()
{
    var classReplyComment =  document.getElementsByClassName('formSendReplyComment');
    for (i = 0; i < classReplyComment.length; i++) {
        classReplyComment[i].innerHTML = "";
        }
}
// function getDate() current
function GetFormattedDate() {
    var todayTime = new Date();
    var month = todayTime.getMonth() + 1;
    var day = todayTime.getDate();
    var year = todayTime.getFullYear();
    var hour = todayTime.getHours();
    var minute = todayTime.getMinutes();
    return month + "/" + day + "/" + year + "  " + hour + ":" + minute;
}
// function click into icon Reply 
function clickReplyMessage(idCommentReply){
    showOrHidenAllFormSend();
    formSendMessage = renderSendCommentForm(idCommentReply);
    document.getElementById('formSendMess_'+ idCommentReply).innerHTML = formSendMessage;
    document.getElementById("message_input_child_"+idCommentReply).addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            clickSendReplyComment(idCommentReply);
        }
    });
}
//show data ListComment
for(comment of listComment){
    if(comment.ReplyCommentId == null) {
        addNewComment(comment.Id,comment.CreatedDate,comment.CreatedBy,comment.CommentContent,comment.Avatar);
    }
    else{
        renderReplyComment(comment.ReplyCommentId,comment.CreatedDate,comment.CreatedBy,comment.CommentContent,comment.Avatar);
    }
}
// function render Send Comment Form
function renderSendCommentForm(idReplyComment) {
    return `<div class="bottom_wrapper clearfix">
    <div class="message_input_wrapper">
        <input class="message_input" id="message_input_child_${idReplyComment}" placeholder="Type your message here..." />
    </div>
    <div class="send_message" onclick="clickSendReplyComment(${idReplyComment})">
        <div class="icon"></div>
        <div  class="text">Send</div>
    </div>
    </div>`;
}   
// function render ReplyComment
function renderReplyComment(idReplyComment,createdDate,createdBy,commentContent,avatar){
    document.getElementById("replycomment_"+idReplyComment).innerHTML += `
    <li>                                             
        <div class="comment-avatar"><img
                src="${avatar}"
                alt=""></div>
        
        <div class="comment-box">
            <div class="comment-head">
                <h6 class="comment-name by-author"><p>${createdBy}</p></h6>
                <span>${createdDate}</span>
            </div>
            <div class="comment-content">
                ${commentContent}
            </div>
        </div>
    </li>
    `
}
// funtion SendReplyComment
function clickSendReplyComment(idReplyComment){
    date = GetFormattedDate();
    commentContent = document.getElementById('message_input_child_'+idReplyComment).value;
    if (commentContent.trim()!=""){
        renderReplyComment(idReplyComment,date,currentUser.Name,commentContent,currentUser.Avatar);
        document.getElementById('message_input_child_'+idReplyComment).value = "";
    }
}
// function Click Button Send Main
function clickSendNewComment(){
    showOrHidenAllFormSend();
    var commentContent = document.getElementById("message_input_main").value;
    idComment = Math.floor(Math.random() * 100000);
    var date = GetFormattedDate();
    if(commentContent.trim()!=""){
        addNewComment(idComment,date,currentUser.Name,commentContent,currentUser.Avatar)
        document.getElementById("message_input_main").value = '';
    }
}
// function Add New Commnent without IdReplyComment
function addNewComment(idComment,createdDate,createdBy,commentContent,avatar) {
    document.getElementById("comments-list").innerHTML += `
    <li>
    <div class="comment-main-level">
        
        <div class="comment-avatar"><img
                src=" ${avatar}"     
                alt=""></div>
       
        <div class="comment-box">
            <div class="comment-head">
                <h6 class="comment-name by-author"><p>${createdBy}</p>
                </h6>
                <span>${createdDate}</span>
                <i class="fa fa-reply" onclick= "clickReplyMessage(${idComment})"></i>
            </div>
            <div class="comment-content">
                ${commentContent}
            </div>
        </div>
    </div>
        <ul class="comments-list reply-list">
            <div id = "replycomment_${idComment}"></div>
            <div class= "formSendReplyComment" id = "formSendMess_${idComment}"></div>
        </ul>
    </li>
    `;
}
document.getElementById('message_input_main').addEventListener('keypress', function (e) {
    if (e.key === 'Enter') {
        clickSendNewComment();
    }
});