// console.log(sessionStorage.getItem("requestList"));
// if (sessionStorage.getItem("requestList") == '') {
//     requestList = [];
// }
// else {
//     requestList = JSON.parse(sessionStorage.getItem("requestList"));
// }
$(function () {  
  $("#fromdate").datepicker({         
    autoclose: true,         
    todayHighlight: true 
  }).datepicker('update', new Date());
});

$(function () {  
  $("#todate").datepicker({         
    autoclose: true,         
    todayHighlight: true 
  }).datepicker('update', new Date());
});

var requestList = [
  {id:3567, Status:"New", CreatedDate: new Date(), UpdatedDate:"-", Server: "apcoa-pez.giantleap.no - 46.255.17.57", Title:"new request open abc", Request:"aaaa", Answer: "", RequestFrom: new Date("2020-08-06"), RequestTo: new Date("2020-08-13")},
  {id:3515, Status:"Open", CreatedDate: new Date(), UpdatedDate:"-", Server: "apcoa-pez.giantleap.no - 46.255.17.57", Title:"new request open abc", Request:"aaaa", Answer: "", RequestFrom: new Date("2020-08-06"), RequestTo: new Date("2020-08-13")},
  {id:3502, Status:"New", CreatedDate: new Date(), UpdatedDate:"-", Server: "apcoa-pez.giantleap.no - 46.255.17.57", Title:"new request open abc", Request:"aaaa", Answer: "", RequestFrom: new Date("2020-08-06"), RequestTo: new Date("2020-08-13")},
  {id:3544, Status:"Open", CreatedDate: new Date(), UpdatedDate:"-", Server: "apcoa-pez.giantleap.no - 46.255.17.57", Title:"new request open abc", Request:"aaaa", Answer: "", RequestFrom: new Date("2020-08-06"), RequestTo: new Date("2020-08-13")},
  {id:3523, Status:"New", CreatedDate: new Date(), UpdatedDate:"-", Server: "apcoa-pez.giantleap.no - 46.255.17.57", Title:"new request open abc", Request:"aaaa", Answer: "", RequestFrom: new Date("2020-08-06"), RequestTo: new Date("2020-08-13")},
];

var idRequest = 1412;

getDateString = (m) => {
  let dateString = m.getDate() + "."+ (m.getMonth() + 1) + "." + m.getFullYear() + " - " + m.getHours() + ":" + m.getMinutes();
  // let dateString = moment(m).format('dd.MM.yyyy - HH:mm');
  return dateString;
}


function viewElement() {
  
  for (re of requestList) {
      tableRender(re);    
  };

}

tableRender = (re) => {
  var tr  = document.createElement("tr");
  var id = document.createElement("th");
  var status = document.createElement("td");
  var CreatedDate = document.createElement("td");
  var UpdatedDate = document.createElement("td");
  var Server = document.createElement("td");
  var totalTitle = document.createElement("td");
  var bTitle = document.createElement("b");
  var endbTitle = document.createElement("b");
  var brTitle = document.createElement("br");
  var bRequest = document.createElement("b");
  var brRequest = document.createElement("br");
  var bAnswer = document.createElement("b");
  var brAnswer = document.createElement("br");
  var RequestFrom = document.createElement("td");
  var RequestTo = document.createElement("td");
  var tdBtn = document.createElement("td");
  var editButton = document.createElement("button");

  id.append(re.id);
  tr.append(id);
  status.append(re.Status);
  tr.append(status);
  CreatedDate.append(getDateString(re.CreatedDate));
  tr.append(CreatedDate);
  UpdatedDate.append(re.UpdatedDate);
  tr.append(UpdatedDate);
  Server.append(re.Server);
  tr.append(Server);
  bTitle.append("Title:");
  totalTitle.append(bTitle);
  totalTitle.append(endbTitle)
  totalTitle.append(re.Title);
  totalTitle.append(brTitle)
  bRequest.append("Request:");
  totalTitle.append(bRequest);
  totalTitle.append(re.Request);
  totalTitle.append(brRequest)
  bAnswer.append("Answer:");
  totalTitle.append(bAnswer);
  totalTitle.append(re.Answer);
  totalTitle.append(brAnswer);
  tr.append(totalTitle);
  RequestFrom.append(getDateString(re.RequestFrom));
  tr.append(RequestFrom);
  RequestTo.append(getDateString(re.RequestTo));
  tr.append(RequestTo);
  editButton.append("Edit");
  editButton.onclick = () => {
    var url = "./requestDetail.html";
    $(location).attr('href', url); // Using this
  }
  tdBtn.append(editButton);
  tr.append(tdBtn);
  document.getElementById("aba").append(tr);
  
}

addNewData = (e) => {
  e.preventDefault();
  let reqTitle = document.getElementById("title").value;
  let fdate = document.getElementById("fromdate").value;
  let ftime = document.getElementById("fromtime").value;
  let tdate = document.getElementById("todate").value;
  let ttime = document.getElementById("totime").value;
  let reqServer = document.getElementById("server").value;
  let reqDescription = document.getElementById("description").value;
  let newData = {
      id: idRequest,
      Status: "New",
      CreatedDate: new Date(),
      UpdatedDate:"-", 
      Server: reqServer, 
      Title: reqTitle, 
      Request:"aaaa", 
      Answer: "", 
      RequestFrom: new Date(fdate + ' ' + ftime), 
      RequestTo: new Date(tdate + ' ' + ttime),
      description: reqDescription
  }
  idRequest += 1;
  requestList.push(newData);
  console.log(requestList);
  $('#newRequestModal').modal('hide');
  tableRender(newData);
  
  return true;
}

// saveData = () => {
//     sessionStorage.setItem("requestList", JSON.stringify(requestList));
// }

